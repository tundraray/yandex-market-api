// Initializes the `images` service on path `/images`
const createService = require('./product.class.js');
const hooks = require('./product.hooks.js');

module.exports = function (app) {
  
  const paginate = app.get('paginate');

  const options = {
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/product', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('product');

  service.hooks(hooks);
};
