/* eslint-disable no-unused-vars  */
/* eslint-disable no-useless-escape  */

const fetch = require('node-fetch');
const _ = require('lodash');

class Service {
  constructor(options) {
    this.options = options || {};
  }

  async find(params) {
    const {
      query: {
        q
      }
    } = params;
    const pageData = await fetch(`https://yandex.ru/suggest-market/suggest-market-rich?v=2&enable-continuation=1&group_sort=max&hl=1&market_personal_num=2&market_personal=1&srv=market&wiz=TrWth&part=${q}&pos=16&svg=1&_=1536180922251`);
    const result = await pageData.json();
    const suggestData = _.get(result, ['2'], []).filter(data => data.includes('sku')).map(item => {
      const {
        img,
        text,
        link
      } = JSON.parse(item)[0];
      const id = _.get(/\/product\/(.*)\?/g.exec(link),['1'],0);
        return {
          img,
          text,
          id
        };
    });
    return _.get(suggestData, ['0'], []);
  }

  async get(id, params) {
    try {
      const pageData = await fetch(`https://market.yandex.ru/product/${id}`);
      const result = await pageData.text();
      const regexImage = /id=\"n-gallery\" data-bem=\"(?<g>.*)\"\>\<div class=\"n-gallery__image-container\"\>/g;
      const regexDescription = /class=\"n-product-summary b-zone i-bem\" data-bem=\"(?<g>.*)\"\>\<div itemscope=\"\" itemtype=\"https:\/\/schema.org\/Product\"/g;
      const regexTitle = /<h1(?<s>.*)\>(?<g>.*)\<\/h1>/g;
      const matchImage = regexImage.exec(result);
      const matchDescription = regexDescription.exec(result);
      const matchTitle = regexTitle.exec(result);

      if (typeof matchImage[1] === 'undefined') {
        return '';
      }
      return {
        id,
        pictures: _.get(JSON.parse(matchImage[1].replace(/&quot;/g, '"')), ['n-gallery', 'data', 'pictures'], []),
        description: _.get(JSON.parse(matchDescription[1].replace(/&quot;/g, '"')), ['n-product-summary', 'description']),
        title: _.get(matchTitle, ['groups', 'g'], '')
      };
    } catch (error) {
      console.log(error);
      return error;
    }

  }
}

module.exports = function (options) {
  return new Service(options);
};

module.exports.Service = Service;
