// Initializes the `offers` service on path `/offers`
const createService = require('./offers.class.js');
const hooks = require('./offers.hooks');

module.exports = function (app) {
  
  const paginate = app.get('paginate');

  const options = {
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/offers', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('offers');

  service.hooks(hooks);
};
