const fs = require('fs');

const saveToJSONFile = (jsonObj, targetFile) => {

  return new Promise((resolve, reject) => {

    try {
      var data = JSON.stringify(jsonObj);

    } catch (err) {
      // eslint-disable-next-line no-console
      console.log('Could not convert object to JSON string ! ' + err);
      reject(err);
    }

    // Try saving the file.        
    fs.writeFile(targetFile, data, (err) => {
      if (err)
        reject(err);
      else {
        resolve(targetFile);
      }
    });

  });
};

const readFromJSONFile = (targetFile) => {

  return new Promise((resolve, reject) => {

    fs.readFile(targetFile, async function (err, data) {

      if (err)
        reject(err);
      else {
        resolve(JSON.parse(data));
      }
    });
  });
};

const cookieObjToString = (cookies, obj) => {
  return cookies.map(({
    name,
    value
  }) => {
    if (obj && obj[name]) {
      return `${name}=${obj[name]}`;
    }
    return `${name}=${value}`;
  }).join(';');
};



module.exports = {
  saveToJSONFile,
  readFromJSONFile,
  cookieObjToString
};
