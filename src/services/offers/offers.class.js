/* eslint-disable no-console */
/* eslint-disable no-unused-vars  */
/* eslint-disable no-useless-escape  */

const fetch = require('node-fetch');
const _ = require('lodash');
const puppeteer = require('puppeteer');
const devices = require('puppeteer/DeviceDescriptors');
const {
  saveToJSONFile,
  readFromJSONFile,
  cookieObjToString
} = require('./cookies');


class Service {
  constructor(options) {
    this.options = options || {};
    
  }


  async get(id, params) {
    let model = null;
    const url = `https://m.market.yandex.ru/product--xren/${id}?local-offers-first=1&how=aprice&deliveryincluded=1`;

    if(!this.browser)
      this.browser = await puppeteer.launch({
        executablePath: '/usr/bin/google-chrome',
        args: ['--no-sandbox', '--disable-dev-shm-usage', '--enable-logging', '--v1=1'],
        handleSIGINT: false,
        handleSIGTERM: false,
      });
    try {
      
      const page = await this.browser.newPage();
      await page.emulate(devices['iPhone 6']);
      const cookies = await readFromJSONFile('./cookie.json');
      await page.goto(url);
      await page.setCookie(...cookies);

      let pageState = await page.evaluate('window.state');
      if (!pageState || !pageState.user)
        pageState = {
          user: {}
        };
      const {
        user: {
          sk,
          yandexuid,
        } = {}
      } = pageState;
      
    

      const cookiesSet = await page.cookies(url);
      await saveToJSONFile(cookiesSet, './cookie.json');
      await page.close();

      
      if(sk) {
        const opts = {
          'credentials': 'include',
          'headers': {
            'accept': '*/*',
            'accept-language': 'ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7',
            'cache-control': 'no-cache',
            'pragma': 'no-cache',
            'x-requested-with': 'XMLHttpRequest',
            'cookie': cookieObjToString(cookiesSet, {
              yandexuid
            }),
            'user-agent': devices['iPhone 6'].userAgent,
          },
          'referrer': 'https://m.market.yandex.ru/product--xren/39068712?local-offers-first=1&how=aprice&deliveryincluded=1&onstock=0&page=1',
          'referrerPolicy': 'no-referrer-when-downgrade',
          'body': null,
          'method': 'GET',
          'mode': 'cors'
  
        };
  
        const result = await fetch(`https://m.market.yandex.ru/api/product/${id}/offers?sk=${sk}&hid=90639&how=aprice&good-state=false&cutprice-filter=true&local-offers-first=1&deliveryincluded=1&skip=0&numdoc=6&offers-set=list%2Cdefault`, opts);
        model = await result.json();
        
      } else {
        return {
          title: await page.title(),
        };
      }
      

    } catch (error) {
      console.error(error);
    }
    
    return model;
  }
}

module.exports = function (options) {
  return new Service(options);
};

module.exports.Service = Service;
