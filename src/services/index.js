const product = require('./product/product.service.js');
const offers = require('./offers/offers.service.js');
// eslint-disable-next-line no-unused-vars
module.exports = function (app) {
  app.configure(product);
  app.configure(offers);
};
